import requests
import json
import os
import sys
from flask import Flask, render_template, request

api_key =  "acc_c01d836bf8e2687"
api_secret = "75e0db9e8a2f58668550db4ae2b4b018"
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/success', methods = ['POST'])
def success():
    if request.method == 'POST':
        f = request.files['file']
        extension = f.filename.split('.')[1]
        name = "temp." + extension;
        f.save(name)
        print(f)
        image_path = "/home/rosh/programming/imagga/" + name
        response = requests.post(
                'https://api.imagga.com/v2/tags',
                auth=(api_key, api_secret),
                files={'image': open(image_path, 'rb')}
                )
        jsonData = response.json()
        things = jsonData["result"]["tags"][0]["tag"]["en"]
        #for tags in jsonData["result"]["tags"]:
        #    things = "%20s : %s " % (tags["tag"]["en"], int(tags["confidence"])) 
        return render_template('success.html',item=things)

if __name__ == '__main__':
    app.run()
